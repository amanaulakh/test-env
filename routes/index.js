var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  //console.log(process.env.OPENSHIFT_BUILD_NAME);
 // alert('test');
  res.render('index', { title: 'Express1 ' + process.env.test1 });
});

module.exports = router;
